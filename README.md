***
#bamboo hr filler
***

## Before running

got to bamboo_hr_filler/constants/user_info.py and configure the following details:

```
class UserInfo:
    id = 19  # make sure your id is correct
    URL = f"https://datagen.bamboohr.com/employees/timesheet/?id={id}"
    USER = "<company_email>"
    PASS = "<password>"
    TOTAL_TIME_START = "8:50"
    TOTAL_TIME_END = "11:00"
    TIME_FORMAT = "%H:%M"
```






## Creating a Virtual Environment for Development:

```
$ sudo brew update $ sudo brew install -y python3-venv
$ python3 -m venv template_venv
$ source template_venv/bin/activate
$ pip3 install --upgrade pip $ python3 setup.py install
```


## Deployment - deb package creation

```
$ sudo ./build.sh <version_number>
```