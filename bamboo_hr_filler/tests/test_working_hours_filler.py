import os
import unittest

import pkg_resources


resource_package = pkg_resources.get_distribution('bamboo_hr_filler').location
blender_3d_hand = os.path.join(
    resource_package, 'bamboo_hr_filler', 'assets', 'creds.txt')


class TestWorkingHoursFiller(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

