import os
import time
from unittest import TestCase

import pkg_resources
from selenium import webdriver

from bamboo_hr_filler.constants.login_info import UserInfo
from bamboo_hr_filler.constants.bamboo_hr_web_fields import BambooHrLoginBoxes

resource_package = pkg_resources.get_distribution("bamboo_hr_filler").location
chrome_driver_path = os.path.join(resource_package, "bamboo_hr_filler", "assets", "chromedriver")


class TestChromeDriver(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        pass

    def test_connect_to_google(self):
        driver = webdriver.Chrome(chrome_driver_path)  # Optional argument, if not specified will search path.
        driver.get("http://www.google.com/")
        time.sleep(3)  # Let the user actually see something!
        search_box = driver.find_element_by_name("q")
        search_box.send_keys("yad2")
        search_box.submit()
        time.sleep(3)  # Let the user actually see something!
        driver.quit()

    def test_connect_to_bammbo_hr(self):
        driver = webdriver.Chrome(chrome_driver_path)  # Optional argument, if not specified will search path.
        driver.get(UserInfo.URL)
        time.sleep(3)  # Let the user actually see something!
        username_box = driver.find_element_by_id(BambooHrLoginBoxes.USERNAME_ID)
        username_box.send_keys(UserInfo.USER)
        password_box = driver.find_element_by_name(BambooHrLoginBoxes.PASSWORD_NAME)
        password_box.send_keys(UserInfo.PASS)
        password_box.submit()
        time.sleep(3)  # Let the user actually see something!
        driver.quit()
