import datetime
import random
import time

from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from webdriver_manager.chrome import ChromeDriverManager

from bamboo_hr_filler.constants.bamboo_hr_web_fields import BambooHrLoginBoxes
from bamboo_hr_filler.constants.time_sheet import TimeSheetFields
from bamboo_hr_filler.constants.user_info import UserInfo


class WorkingHoursFiller:
    def __init__(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.year = datetime.datetime.now().year
        self.month = datetime.datetime.now().month

    def run(self):
        try:
            self.driver.get(UserInfo.URL)
            WorkingHoursFiller._wait()
            self._log_in()
            WorkingHoursFiller._wait()

            time_sheet_slats = self.driver.find_elements_by_xpath(TimeSheetFields.TIME_SHEETS_XPATH)

            WorkingHoursFiller._wait()

            for time_sheet_slat in time_sheet_slats:

                if not time_sheet_slat.text:
                    continue

                day_of_week, month = self._get_slat_date(time_sheet_slat)
                if not self._is_working_day(month=month, day_of_week=day_of_week):
                    continue

                if not self._is_empty(time_sheet_slat):
                    continue

                self.click_on_entry_link(time_sheet_slat)
                self.fill_slat(time_sheet_slat)
            self._save_changes()

            self._wait()
            self.driver.quit()
        except Exception as ex:
            print(ex)

    def click_on_entry_link(self, time_sheet_slat):
        WorkingHoursFiller._wait()
        element = self.get_working_hours_element(time_sheet_slat)
        try:
            element.click()
        except WebDriverException:
            self.driver.execute_script("arguments[0].click();", element)
        finally:
            WorkingHoursFiller._wait()

    def fill_slat(self, time_sheet_slat):
        element = self.get_working_hours_element(time_sheet_slat)
        random_working_hours = WorkingHoursFiller._get_random_working_hours()
        element.send_keys(random_working_hours)

    @staticmethod
    def _wait():
        time.sleep(2)

    def _save_changes(self):
        save_button = self.driver.find_elements_by_class_name(TimeSheetFields.BUTTON_FAB_CLASS)[2]
        save_button.click()

    @staticmethod
    def _get_random_working_hours():
        random_time = WorkingHoursFiller.get_random_time(start=UserInfo.TOTAL_TIME_START, end=UserInfo.TOTAL_TIME_END)
        return time.strftime(UserInfo.TIME_FORMAT, time.localtime(random_time))

    @staticmethod
    def get_random_time(start, end):
        start_time = time.mktime(time.strptime(start, UserInfo.TIME_FORMAT))
        end_time = time.mktime(time.strptime(end, UserInfo.TIME_FORMAT))
        random_time = start_time + random.random() * (end_time - start_time)
        return random_time

    def _get_slat_date(self, time_sheet_slat):
        day_of_week = time_sheet_slat.find_element_by_class_name(TimeSheetFields.DAY_OF_WEEK_CLASS).text
        day_date = time_sheet_slat.find_element_by_class_name(TimeSheetFields.DAY_DATE_CLASS).text
        month = datetime.datetime.strptime(f"{day_date} {self.year}", "%b %d %Y").month
        return day_of_week, month

    def _is_empty(self, time_sheet_slat):
        working_hours_value = self.get_working_hours_element(time_sheet_slat).get_attribute("value")
        if working_hours_value:
            return False
        return True

    @staticmethod
    def get_working_hours_element(time_sheet_slat):
        return time_sheet_slat.find_element_by_class_name(TimeSheetFields.INPUT_CLASS)

    def _is_working_day(self, month, day_of_week):

        if day_of_week == "Fri" or day_of_week == "Sat":
            return False

        if month != self.month:
            return False

        return True

    def _log_in(self):
        username_box = self.driver.find_element_by_id(BambooHrLoginBoxes.USERNAME_ID)
        username_box.send_keys(UserInfo.USER)
        password_box = self.driver.find_element_by_name(BambooHrLoginBoxes.PASSWORD_NAME)
        password_box.send_keys(UserInfo.PASS)
        password_box.submit()
