class TimeSheetFields:
    TIME_SHEETS_XPATH = "//form/div"
    INPUT_CLASS = "TimesheetSlat__input"
    BUTTON_FAB_CLASS = "fab-Button"
    DAY_OF_WEEK_CLASS = "TimesheetSlat__dayOfWeek"
    DAY_DATE_CLASS = "TimesheetSlat__dayDate"

