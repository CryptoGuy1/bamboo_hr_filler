class UserInfo:
    id = 19  # make sure your id is correct
    URL = f"https://datagen.bamboohr.com/employees/timesheet/?id={id}"
    USER = "<company_email>"
    PASS = "<password>"
    TOTAL_TIME_START = "8:50"
    TOTAL_TIME_END = "11:00"
    TIME_FORMAT = "%H:%M"
