#!/bin/bash

# For Debian based destributions only!
# This script is made for creating a debian package

rm *.deb
rm *.tar.gz
apt-get install python3-stdeb fakeroot python-all
python3 setup.py --command-packages stdeb.command sdist_dsc --dist-dir=template_deb bdist_deb
cp preinst template_deb/template-$1/debian/
cd template_deb/template-$1/
dpkg-buildpackage -rfakeroot -uc -us -b
cd ../../
cp template_deb/*deb ./
rm -rf template_deb/

