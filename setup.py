#!/usr/bin/env python3

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(
    name='bamboo_hr_filler',
    version='1.0.1',
    description='template project to easily start a python3 project',
    longdescription=readme(),
    packages=['bamboo_hr_filler'],
    package_data={'bamboo_hr_filler': ['assets/*']},
    url='',
    python_requires=">=3.7",
    license='',
    zip_safe=False,
    author='Guy Milrud',
    author_email='guy.milrud@datagen.tech',
    scripts=['bin/run'],
    install_requires=['']

)
